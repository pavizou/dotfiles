#!/bin/bash -u
#
# Install and configure Zsh, oh-my-zsh and dotfiles.
#
# @deps : apt-get, sudo, git

echo "Install .dotfiles..."

KRABBY_VERSION=0.1.8
NVM_VERSION=0.39.7
ZSH_CUSTOM=$HOME/.oh-my-zsh/custom

refresh_package_list() {
  if command -v apt &> /dev/null
  then
    sudo apt update
  elif command -v dnf &> /dev/null
  then
    sudo dnf updateinfo
  elif command -v pacman &> /dev/null
  then
    sudo pacman -Sy
  else
    echo "Could not find supported package manager"
  fi
}

install_package() {
  if command -v apt &> /dev/null
  then
    sudo apt install -y $@
  elif command -v dnf &> /dev/null
  then
    sudo dnf install --assume-yes $@
  elif command -v pacman &> /dev/null
  then
    sudo pacman -S --noconfirm $@
  else
    echo "Could not find supported package manager"
  fi
}

# Install Zsh
if [ $(which zsh &> /dev/null; echo $?) -eq 1 ]
then
  touch $HOME/.zshrc
  install_package zsh
  sudo chsh -s `which zsh` `whoami`
fi

# Clone or update oh-my-zsh
ohMyZshDir=$HOME/.oh-my-zsh
if [ ! -d $ohMyZshDir ]; then
  git clone -q https://github.com/robbyrussell/oh-my-zsh.git $ohMyZshDir
  # curl https://raw.githubusercontent.com/caiogondim/bullet-train-oh-my-zsh-theme/master/bullet-train.zsh-theme -o $ZSH_CUSTOM/themes/bullet-train.zsh-theme
else
  git --git-dir=$ohMyZshDir/.git --work-tree=$ohMyZshDir pull -q --rebase
fi

echo "Cloning fzf-tab plugin"
git clone https://github.com/Aloxaf/fzf-tab ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/fzf-tab
echo "Cloning fast-syntax-highlighting plugin"
git clone https://github.com/zdharma-continuum/fast-syntax-highlighting.git \
  ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/fast-syntax-highlighting

# Backup git user info
gitUserName=$(git config --global user.name)
gitUserEmail=$(git config --global user.email)

# Copy all dotfiles
cp -rf `pwd`/dotfiles/.[a-z]* $HOME/

echo "Refreshing package list before installing additonnal packages"
refresh_package_list

echo "Installing build tools"
install_package gcc make

echo "Installing Starship prompt 🚀"
curl -sS https://starship.rs/install.sh | sh

echo "Installing Neovim"
install_package neovim

echo "Installing Ripgrep"
install_package ripgrep

echo "Installing fd"
install_package fd

echo "Installing fzf"
install_package fzf

echo "Installing eza"
install_package eza

echi "Installing Zoxide"
install_package zoxide

echo "Installing Lazygit"
if command -v pacman &> /dev/null
then
  install_package lazygit
elif command -v dnf &> /dev/null
then
  sudo dnf copr enable atim/lazygit -y
  install_package lazygit
else
  LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*')
  curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"
  tar xf lazygit.tar.gz lazygit
  sudo install lazygit /usr/local/bin
fi

echo "Installing Tmuxp"
install_package tmuxp

echo "Installing Mkcert"
install_package mkcert
echo "Installing Mkcert's root CA"
mkcert -install

pyversion=$(python -V 2>&1 | grep -Po '(?<=Python )(.+)')
if [[ -z "$pyversion" ]]
then
    echo "Python is not installed"
else
  parsedVersion=$(echo "${pyversion//./}")
  if [[ "$parsedVersion" -gt "370" ]]
  then
      echo "Detected compatible Python version ($pyversion). Installing Poetry"
      curl -sSL https://install.python-poetry.org | python3 -
      echo "Enabling completions for Poetry"
      mkdir $ZSH_CUSTOM/plugins/poetry
      poetry completions zsh > $ZSH_CUSTOM/plugins/poetry/_poetry
  else
      echo "Python version ($pyversion) too old for installing Poetry, or it is already installed"
  fi
  echo "Installing Pyenv"
  curl https://pyenv.run | bash
fi

echo "Installing NVM"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v${NVM_VERSION}/install.sh | bash

# Copy all utils scripts
mkdir -p $HOME/bin && cp -f `pwd`/bin/* $HOME/bin

arch=$(uname -m)
if [ "$arch" != 'x86_64' ];
then
  echo "Not x86_64 architecture, skipping Krabby install"
else
  echo "Installing Krabby"
  curl -L https://github.com/yannjor/krabby/releases/download/v${KRABBY_VERSION}/krabby-${KRABBY_VERSION}-x86_64.tar.gz | tar zxf - -C $HOME/bin
fi

## Only install if we are NOT running in a containerized environement
if [[ "$(systemd-detect-virt)" == "none" || "$(systemd-detect-virt)" == "kvm" ]]
then
  DISTRO=$(. /etc/os-release && echo "$ID")
  echo "Installing Docker"
  if command -v apt &> /dev/null
  then
    echo "  --- Allowing HTTPS repos"
    install_package ca-certificates curl gnupg
    echo "  --- Adding Docker's GPG key"
    sudo install -m 0755 -d /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/$DISTRO/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    sudo chmod a+r /etc/apt/keyrings/docker.gpg
    echo "  --- Adding Docker repos"
    echo \
      "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/$DISTRO \
      "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
      sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    echo "  --- Installing Docker"
    refresh_package_list
    install_package docker-ce docker-ce-cli containerd.io docker-compose-plugin
  elif command -v dnf &> /dev/null
  then
    echo "  --- Adding Docker repos"
    sudo dnf -y install dnf-plugins-core
    sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
    echo "  --- Installing Docker"
    install_package docker-ce docker-ce-cli containerd.io docker-compose-plugin
  elif command -v pacman &> /dev/null
  then
    install_package docker docker-compose
  fi

  echo "Installing Podman"
  install_package podman

  echo "Installing Distrobox"
  install_package distrobox
else
  echo "Virtualization detected. Skipping install of Docker, Distrobox and Podman"
fi

# Restore git user info
git config --global user.name $gitUserName
git config --global user.email $gitUserEmail

[ "x$gitUserName" = "x" ] && \
  echo "Please update your git user.name, user.email : vi $HOME/.gitconfig"

zsh

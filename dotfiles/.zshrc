#
# ZSH configuration propulsed by https://github.com/robbyrussell/oh-my-zsh
#

ZSH=$HOME/.oh-my-zsh
# ZSH_THEME="pure-thb"

#CASE_SENSITIVE="true"
DISABLE_AUTO_UPDATE="true"
DISABLE_UPDATE_PROMPT="true"
COMPLETION_WAITING_DOTS="true"
ZOXIDE_CMD_OVERRIDE="cd"
# export FZF_DEFAULT_OPTS='--tmux 100%,50% --border'

### fzf-tab configuration ###
# preview directory's content with eza when completing cd
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'eza -1 --color=always $realpath'
#zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
#zstyle ':fzf-tab:complete:z:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complete:*' fzf-flags '--tmux' '100%,50%'
zstyle ':fzf-tab:*' switch-group '<' '>'

### NVM plugin configuration ###
zstyle ':omz:plugins:nvm' lazy yes
zstyle ':omz:plugins:nvm' autoload yes

plugins=(git docker docker-compose history-substring-search poetry rsync fzf ssh zoxide fzf-tab fast-syntax-highlighting nvm)
source $ZSH/oh-my-zsh.sh

export HISTSIZE=100000
export PATH=~/bin:/usr/games:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/home/pierre/.local/bin:$HOME/.local/share/JetBrains/Toolbox/scripts

##########################

# Thefuck alias
# eval $(thefuck --alias)

# Tiny aliases
alias a='ansible'
alias c='clear'
alias d='docker'
alias dc='docker compose'
alias dm='docker-machine'
alias g='git'
alias h='history'
alias m='make'
alias s='ssh'
alias nsd='npm run start:dev'

# Git aliases
alias gs='git status'
alias gst='git status'
alias gpr='git pull --rebase'
alias grrh='git reset --hard HEAD'
alias gcdf='git clean -df'
alias gcam='git commit --amend'
alias gcamne='git commit --amend --no-edit'
alias gspp='git stash && git pull --rebase && git stash pop'
alias glg='git lg --stat -C -4'
alias grf='git co master && git fetch upstream && git rebase upstream/master && git co dev && git rebase upstream/dev'
gri() { git rebase -i HEAD~$1; }

# Docker aliases
alias dkd="docker run -d -P"
alias dki="docker run --rm -P -ti"
alias dclean='~/bin/docker-cleanup.sh'
alias dcx="docker context"
alias dcxc="docker context create"
alias dcxu="docker context use"
alias dcxd="docker context rm"
alias dcxl="docker context ls"
alias dcxs "docker context show"
db()   { docker build --rm -t="$1" .; }
drmd()  { docker rm $(docker ps -qa); }
drme() { docker rm $(docker ps -qa --filter 'status=exited'); }
dri()  { docker rmi $(docker images -q --filter "dangling=true"); }
dka()  { docker rm -f $(docker ps -aq) }
dgo()  { docker exec -ti $@ sh }
dgob() { docker exec -ti $@ bash }
dip()  { docker inspect --format '{{ .NetworkSettings.IPAddress }}' "$@"; }
dpid() { docker inspect --format '{{ .State.Pid }}' "$@"; }
dim()  { docker images | grep $@; }
dstats() { docker stats $(docker ps | grep -v CON | sed "s/.*\s\([a-z].*\)/\1/" | awk '{printf $1" "}'); }
dvrm() { docker volume rm $(docker volume ls -qf dangling=true) }
ddv() { docker volume ls -qf dangling=true }
# Apt
alias get='sudo apt-get install'
alias gety='sudo apt-get install -y'
alias search='sudo apt-cache search'

####
# Other aliases

alias btreset="sudo rfkill block wlan && sudo modprobe -r btusb && sleep 10 && sudo modprobe btusb && systemctl --user restart pipewire && sudo systemctl restart bluetooth"

# Tmux with terminal supports 256 colours
alias tmux='tmux -2'

# Display listened ports
alias wholisten='sudo netstat -antulp | grep LISTE'

####
# Helpful functions

# @help randpwd $length
randpwd() {
  local length=${1:-42}
  cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $length | head -1
}

# @help always $cmd $sleepDuration
always() { while true; do "$1"; sleep $2; done; }

# @help grepcode $path $fileExtension $grepRegexp
grepcode() { find $1 -name "*.$2" | xargs grep -Hn $3; }

# cURL functions
cuurl() { curl $@ -w "\n@status=%{response_code}\n@time=%{time_total}\n"; }
jc()    { curl -s "$1" | jq .; }
cl()    { curl -s localhost:$@; }
jcl()   { curl -s localhost:$@ | jq .; }

# Update dotfiles
updot() { cd ~/.dotfiles; git pull --rebase; ./install.sh; }

# Display the IP and geo information of the current machine
geoip() { curl -s www.telize.com/geoip | jq .; }

# Update a specific apt repo
# @help update_repo docker
update_repo() {
  sudo apt-get update -o Dir::Etc::sourcelist="sources.list.d/$1.list" \
    -o Dir::Etc::sourceparts="-" -o APT::Get::List-Cleanup="0"
}

create_push_repo() {
  curl -s https://gist.githubusercontent.com/thbkrkr/d37ea4a4f912286ceb9b/raw/cce043b32a14b023868928a728aecf1f7c820b7b/prepare-pushgitrepo.sh | sh -s $1
}

####
# Docker functions

--docker-socket() {
  echo "-v /var/run/docker.sock:/var/run/docker.sock"
}

--docker() {
  echo "-v /usr/bin/docker:/usr/bin/docker"
}

push() {
  declare name=$1
  declare repo=$2
  docker tag -f $name $repo/$name
  docker push $repo/$name
}

export WORKON_HOME=~/.virtualenvs
# pip should only run if there is a virtualenv currently activated
export PIP_REQUIRE_VIRTUALENV=true
# source /usr/local/bin/virtualenvwrapper.sh
##########################

# Source optional ~/.myzshrc
[ -f ~/.myzshrc ] && source ~/.myzshrc

test -e $HOME/.cargo/bin && export PATH="$HOME/.cargo/bin:$PATH"

# test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

#export LC_ALL=C.utf8
export PATH=$PATH:/home/pierre/.spicetify
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
eval "$(starship init zsh)"
krabby random

# bun completions
[ -s "/home/pierre/.bun/_bun" ] && source "/home/pierre/.bun/_bun"

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"

# Go
export PATH=$PATH:/usr/local/go/bin
